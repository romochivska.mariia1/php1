<?php


$gasStation = [
    'code' => null,
    'adress' => null,
    'firm' => null,
    'price' => null,
    'fuel' => null,
    'litre' => null,

];

$stations = [ [


    'code' => 1,
    'adress' => 'Italy',
    'firm'=> 'SHELL',
    'price' => 61,
    'fuel' => 'dp',
    'litre' => 300,
],
    [
        'code' => 2,
        'adress' => 'Germany',
        'firm'=> 'GLUSCO',
        'price' => 47,
        'fuel' => 'a-98',
        'litre' => 500,
    ],

    [
        'code' => 3,
        'adress' => 'France',
        'firm'=> 'UPG',
        'price' => 45,
        'fuel' => 'dp',
        'litre' => 900,
    ],

    [
        'code' => 4,
        'adress' => 'Spain',
        'firm'=> 'WOG',
        'price' => 42,
        'fuel' => 'a-92',
        'litre' => 250,
    ],
    [
        'code' => 5,
        'adress' => 'Spain',
        'firm'=> 'SHELL',
        'price' => 49,
        'fuel' => 'a-98',
        'litre' => 250,
    ],
    [
        'code' => 6,
        'adress' => 'Slovakia',
        'firm'=> 'Socar',
        'price' => 45,
        'fuel' => 'a-95',
        'litre' => 250,
    ],
];




$stations = array_filter($stations,function ($element){
    if($element['litre']<= 900){
        return true;
    }
    return false;
});
include 'table.phtml';




